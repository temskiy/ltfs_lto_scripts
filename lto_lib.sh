#!/usr/bin/env bash

SCRIPT=$(basename -- "$0")

DEV_ROBOT="$(sg_scan | grep "id=$(lsscsi |grep mediumx|awk -F ':' '{print $3}')" | awk '{print $1}'|tr -d ':')"
DEV_LTO="$(sg_scan | grep "id=$(lsscsi |grep tape|awk -F ':' '{print $3}')" | awk '{print $1}'|tr -d ':')"
LTFS_FORMAT_OPTIONS="-f -t -b 1048576"
LTFS_MOUNT_OPTIONS="-o devname=$DEV_LTO -o rw -o scsi_append_only_mode=off -o max_write=1 -o noatime"
LTFS_MOUNT_DIR="/mnt/lto"

red=$(tput setaf 1)
green=$(tput setaf 2)
reset=$(tput sgr0)

function print_hw() {
    echo -e "${green}Robot: ${red}${DEV_ROBOT}${reset}"
    echo -e "${green}Drive: ${red}${DEV_LTO:-not found or locked}${reset}"
}

function print_status() {
    mtx -f $DEV_ROBOT status
}

function check_drive_is_locked() {
    if [ -z ${DEV_LTO+x} ];then
        print_hw
        exit 1
    fi
}

function eject() {
    check_drive_is_locked
    echo -ne "${green}Rewinding... Please wait..."
    mtx -f $DEV_ROBOT unload $1 > /dev/null 2>&1
    while [[ $?  -gt 0 ]]
    do
        echo -ne "... "
        sleep 5
        mtx -f $DEV_ROBOT unload $1 > /dev/null 2>&1
    done
    echo -e "${reset}"
}

function format() {
    check_drive_is_locked
    if [ -z ${1+x} ];then
        echo -ne "${green}Barcode: ${reset}"
        read barcode
    else
        barcode=$1
    fi
    if [ -z ${2+x} ];then
        echo -ne "${green}Description: ${reset}"
        read description
    else
        description=$2
    fi
    mkltfs $LTFS_FORMAT_OPTIONS -d $DEV_LTO -s "$barcode" -n "$description"
}

function insert() {
    check_drive_is_locked
    if [ -z ${1+x} ];then
        print_status
        echo -ne "${green}Enter slot numbet to load: ${reset}"
        read slot_number
    else
        slot_number=$1
    fi
    mtx -f $DEV_ROBOT load $slot_number
}

function ltfs_mount() {
    check_drive_is_locked
    ltfs $LTFS_MOUNT_OPTIONS $LTFS_MOUNT_DIR
}

function ltfs_umount() {
    umount $LTFS_MOUNT_DIR
    ec = $?
    if [[ $ec -gt 0 ]];then
        exit $ec
    fi
}

function move() {
    check_drive_is_locked
    if [ -z ${1+x} ];then
        print_status
        echo -ne "${green}Transfer from slot: ${reset}"
        read slot_from
    else
        slot_from=$1
    fi

    if [ -z ${2+x} ];then
        echo -ne "${green}Transfer to slot: ${reset}"
        read slot_to
    else
        slot_to=$2
    fi
    mtx -f $DEV_ROBOT transfer $slot_from $slot_to

}

case $SCRIPT in
"lib-status")
    print_hw
    print_status
;;
"ltfs-format")
    format $1 $2
;;
"ltfs-mount")
    ltfs_mount
;;
"ltfs-umount")
    ltfs_umount
;;
"lto-eject")
    eject $1
;;
"lto-im")
    insert $1
    ltfs_mount
;;
"lto-insert")
    insert $1
;;
"lto-move")
    move $1 $2
;;
"lto-ue")
    ltfs_umount
    eject $1
;;
*)
    print_hw
    echo $SCRIPT
;;
esac