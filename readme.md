Скрипты для работы с LTO библиотекой/LTO приводом
=================================================

Основной весь функционал в lto_lib.sh все остальные *.sh - оберки для удобного вызова lto_lib.sh 


Зависимости
-----------
1. [sg3_utils](https://github.com/hreinecke/sg3_utils) - набор утилит для работы с SCSI

2. lsscsi - тоже утилита для работы с SCSI, ставится любым удобным способом

3. [ltfs](https://github.com/LinearTapeFileSystem/ltfs) - собственно инструменты для работы с ltfs